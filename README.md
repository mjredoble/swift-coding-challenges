# Swift-Coding-Challenges



## Strings
 - Challenge 1: Are the letters unique?
 - Challenge 2: Is a string a palindrome?
 - Challenge 3: Do two strings contain the same characters?
 - Challenge 4: Does one string contain another?
 - Challenge 5: Count the characters
 - Challenge 6: Remove duplicate letters from a string
 - Challenge 7: Condense whitespace
import Foundation

// MARK: - Strings Challenge #1: Are the letters unique?
func areLettersUnique(input: String) -> Bool {
    var duplicates = Set<Character>()
    
    var result = true
    let arr = Array(input)
    for indx in 0..<input.count {
        let next = indx + 1
        let letter = arr[indx]
        print("Letter: \(letter)")
        
        for indx2 in next..<input.count {
            if letter == arr[indx2] {
                //print("-----> \(letter) == \(arr[indx2])")
                duplicates.insert(letter)
                result = false
            }
        }
    }
    
    print("Duplicates: \(duplicates)")
    return result
}

func challenge1a(input: String) -> Bool {
    var usedLetters = [Character]()
    
    for letter in input {
        if usedLetters.contains(letter) {
            return false
        }
        
        usedLetters.append(letter)
    }
    
    return true
}

func challenge1b(input: String) -> Bool {
    return Set(input).count == input.count
}

// MARK: - Strings Challenge #2: Is a string a palindrome?
func isAPalindrome(input: String) -> Bool {
    var palindrome = [Character]()
    for letter in input.reversed() {
        palindrome.append(letter)
    }
    
    return String(palindrome).lowercased() == input.lowercased()
}

func challenge2(input: String) -> Bool {
    let lowercase = input.filter { !$0.isWhitespace }.lowercased()
   return lowercase.reversed() == Array(lowercase)
}

// MARK: - Challenge #3: Do two strings contain the same characters?
func twoSameStrings(input1: String, input2: String) -> Bool {
    var common = [Character]()
    var result = false
    
    for chr1 in input1 {
        for chr2 in input2 {
            if chr1 == chr2 {
                common.append(chr2)
            }
        }
    }
    
    if common.count == input1.count {
        result = true
    }
    
    return result
}

func challenge3b(string1: String, string2: String) -> Bool {
    let array1 = Array(string1)
    let array2 = Array(string2)
    return array1.sorted() == array2.sorted()
}

// MARK: - Challenge #4: Does one string contain another?
extension String {
    func fuzzyContains(_ string: String) -> Bool {
        return range(of: string, options: .caseInsensitive) != nil
    }
}

// MARK: - Challenge #5: Count the characters -
// Write a function that accepts a string, and returns how many times a specific character appears, taking case into account.
func characterCount(input: String, count: Character) -> Int {
    var result = 0
    
    for item in input {
        if count == item {
            result += 1
        }
    }
    
    return result
}

func reduceExample() {
    let numbers = [1, 0, 3, 4]
    
    //0 inside reduce(0) is starting value to use
    let numberSum = numbers.reduce(0) { x, y in
        print("x:\(x)")
        print("y:\(y)")
        return x + y
    }
}

func challenge5b(input: String, count: Character) -> Int {
    return input.reduce(0) {
        $1 == count ? $0 + 1 : $0
    }
}

func challenge5d(input: String, count: Character) -> Int {
    let modified = input.replacingOccurrences(of: input, with: "")
    return input.count - modified.count
}

// MARK: - Challenge #6: Challenge 6: Remove duplicate letters from a string
func removeDuplicateLetters(input: String) {
    var text = [Character]()
    for item in input {
        if !text.contains(item) {
            text.append(item)
        }
    }
    
    print(String(text))
}

// MARK: - Challenge #7: Challenge 6: Condense whitespace
func condenseSpaces(input: String) {
    let x = input.replacingOccurrences(of: " ", with: "-")
    print(x)
}

func challenge3a(string1: String, string2: String) -> Bool {
    var checkString = string2
    for letter in string1 {
        if let index = checkString.firstIndex(of: letter) {
            checkString.remove(at: index)
        } else {
            return false
        }
    }
    return checkString.count == 0
}

